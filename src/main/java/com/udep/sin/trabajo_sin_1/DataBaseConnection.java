//Este archivo conecta la aplicación Web con la BD
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.udep.sin.trabajo_sin_1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Gonzalo
 */
public class DataBaseConnection {
    protected static Connection initializeDatabase()
        throws SQLException, ClassNotFoundException
    {
        Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/trabajo_sin?user=root&password=");
        return con;
    }
}
