/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.udep.sin.trabajo_sin_1;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;


/**
 *
 * @author Gonzalo
 */
public class ServletBusqueda extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            //creamos variable para el texto de POST ingresado en busqueda.html
            String texto_Post = request.getParameter("textBusqueda");
            
            // Initialize the database
                //Paso No. 1 establecimiento de la conexion
                Connection con = DataBaseConnection.initializeDatabase();
                //Paso No. 2 preparamos la sentencia SQL
                Statement stmt = con.createStatement();
                //Paso No. 3 Obtenemos los registros que coincidan con el texto_Post
                ResultSet rs = stmt.executeQuery("SELECT * FROM productos WHERE nombre='" +texto_Post +"'");
            
            // html
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletBusqueda</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet del archivo: " + request.getContextPath() + "</h1>");
            
            out.println("Palabra buscada: " + texto_Post);
                        
            //tabla de datos
            out.println("<table border=1 width=50% height=50%>");
            out.println("<tr><th>Id</th><th>Producto</th><th>Descripción</th><tr>");

                while (rs.next())
                {
                    
                        int n = rs.getInt("id");
                        String nm = rs.getString("nombre");
                        String s = rs.getString("descripcion");
                        
                        out.println("<tr><td>" + n + "</td><td>" + nm + "</td><td>" + s + "</td><tr>");
                    
                }
            
            out.println("</body>");
            out.println("</html>");
            
        con.close();
        
        } 
        // Colocamos este catch para que aguante un posible error en el try
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    /*
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    */

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
